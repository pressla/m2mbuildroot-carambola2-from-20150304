#!/bin/sh

if [ $# -ne 1 ]
then
  echo "Usage: autocopy.sh </absolute/path/file>"
  exit 1
fi

line=`ifconfig eth1 | grep "inet addr"`;
myip=`echo $line | cut -d " " -f 2 | cut -d ":" -f 2`

/usr/sbin/nvram get smesh_tree | while read line
do
        printf "\nCopying from $myip to $line..."
        scp $1 -i /jffs/dropbear.priv root@$line:$1
done

echo "#!/bin/sh" > /jffs/neighbors_tmp

/usr/sbin/nvram get smesh_tree | while read line
do
        echo "dbclient -i /jffs/dropbear.priv root@$line /jffs/autocopy.sh $1" >> /jffs/neighbors_tmp
done

chmod a+x /jffs/neighbors_tmp

#echo "start executing on $myip"
/jffs/neighbors_tmp
#echo "stop executing on $myip"
rm /jffs/neighbors_tmp
