#!/bin/sh

if [ $# -ne 1 ]
then           
        echo "Usage: autocommand.sh <command>"
        exit 1
fi            

uptime=`cat /proc/uptime | cut -d " " -f 1`
myname=`uname -a | cut -d " " -f 2`

printf "\n$myname:\n"

echo "#!/bin/sh" > /tmp/autocommand_tmp
echo $1 >> /tmp/autocommand_tmp
chmod a+x /tmp/autocommand_tmp
/tmp/autocommand_tmp
rm /tmp/autocommand_tmp

echo "#!/bin/sh" > /tmp/neighbors_tmp
                                     
/usr/sbin/nvram get smesh_tree | while read line 
do                                  
        echo "dbclient -i /jffs/dropbear.priv root@$line \"/jffs/autocommand.sh \\\"$1\\\"\"" >> /tmp/neighbors_tmp
done                                                                                                    
                                                                                                        
chmod a+x /tmp/neighbors_tmp
/tmp/neighbors_tmp          
rm /tmp/neighbors_tmp

