#!/bin/bash

# The path of this script so that it can run on a cron
SMESH_LIVE_DIR=/directory/bla

# The IP address, in the external port,  of the root Internet gateway
# on the mesh
SMESH_GATEWAY_IP=<ip_address>

cd $SMESH_LIVE_DIR
scp -P 22 root@smesh-gw:/tmp/smesh.snapshot .
if [ $? -ne 0 ]; then
    scp -P 722 root@smesh-gw:/tmp/smesh.snapshot .
fi
scp -P 22 root@smesh-gw:/tmp/spines.snapshot .
if [ $? -ne 0 ]; then
    scp -P 722 root@smesh-gw:/tmp/spines.snapshot .
fi
J=`date +"%s"`
J=`expr $J % 30`

chmod 666 *.snapshot
$SMESH_IVE_DIR/parse_logs.pl smesh.snapshot spines.snapshot > smesh_live
dot -Gepsilon=0.0001 -Gratio="0.8" -Tpng graph > smesh_graph.png

# Copy generated files into website directory
cp smesh_graph.png /web/www.smesh.org/htdocs
cp smesh_live /web/www.smesh.org/htdocs

# Need to touch html file that includes the live image so that
# browsers will re-download the image.
touch /web/www.smesh.org/htdocs/live.html

