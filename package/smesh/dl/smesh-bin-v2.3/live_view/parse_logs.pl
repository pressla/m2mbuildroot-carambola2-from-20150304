#!/usr/bin/perl

# put nice names to each client
%known_ip = (
		"10.83.77.181"	=> 'krypton',
);

open(IN, "$ARGV[0]");

$line = <IN>;

%clients = ();

while ($line ne '') {
#@2005-09-18 13:12:09.368619 log: 1129641129:354604 10.0.11.31 lq_entry: sender_id: 10.0.11.31 linkq: 7 timeval: 1309597 966030 [10.144.80.74] LR: 0
#log: 1131042634:678956 10.0.11.31 Client Edge: 10.0.11.31 -> 10.14.47.32 : 0   Quality: 30   GS: 1   LR: 0
	if ($line =~ /log: ([^ ]*) ([^ ]*) Client Edge: ([^ ]*) -> ([^ ]*) : ([^ ]*) [ ]* Quality: ([^ ]*)/ ) {
		$timestamp = $1;
		$sender_ip = $2;
		$real_sender_ip = $3;
		$linkq = $6;
		$client_ip = $4;
		$datagroup = $5;
		
		$linkq_short = (int $linkq * 10) / 10;
		$clients{$client_ip}{$real_sender_ip} = "$linkq_short $datagroup";
	}
	
	$line = <IN>;
}

close(IN);

print `date`;
print "\n";
print "SMESH live update:\n";
print "------------------\n";

open($OUT, ">graph");
# the wired nodes and links between gateways when hybrid routers are enbabled 
# must be manually added here so that they have a different color..cannot tell
# the difference from the log otherwise.
print $OUT "graph smesh {
        node [style=box, style=filled, color=black, fillcolor=steelblue1 fontsize=9];
		\"10.0.11.31\" [style=filled, color=black, fillcolor=blue label=\"10.0.11.31\" fontcolor=white fontsize=9];
		\"10.0.11.21\" [style=filled, color=black, fillcolor=blue label=\"10.0.11.21\" fontcolor=white fontsize=9];
        \"10.0.11.31\" -- \"10.0.11.21\" [color=blue2 style=bold];
";


$rclients = \%clients;
for $client (keys %$rclients) {
	print "\nCLIENT $client:\n";
	print $OUT "\t\"$client\" [shape=box, style=\"filled\", color=black, fillcolor=seagreen1 label=\"$known_ip{$client}\\n$client\" fontsize=9];\n";

	for $ip (keys %{$rclients->{ $client }}) {
		$new_state = $rclients->{ $client }{ $ip };
		if ($new_state =~ /([^ ]*) ([^ ]*)/) {
			$link_quality = $1;
			$data_group = $2;
		}

		print "\tip = $ip\t linkq = $link_quality\n";
		if ($data_group == 0) {
			print $OUT "\t \"$client\" -- \"$ip\" [label=$link_quality style=dashed color=green fontsize=9];\n";
		} else {
			print $OUT "\t \"$client\" -- \"$ip\" [label=$link_quality color=green style=bold fontsize=9];\n";
		}
	}
}


# parse Spines logs 

open(IN, "$ARGV[1]");

$line = <IN>;

#@2005-09-18 16:43:22.201566 
#@2005-09-18 16:43:22.202470 Available edges:
#@2005-09-18 16:43:22.203537             10.0.11.31 -> 10.0.11.1 :: 1 | 1623:316490
#@2005-09-18 16:43:22.204495             10.0.11.1 -> 10.0.11.23 :: 1 | 1129653116:780803
#@2005-09-18 16:43:22.205425             10.0.11.1 -> 10.0.11.31 :: 1 | 1129653116:689650
#@2005-09-18 16:43:22.206419             10.0.11.23 -> 10.0.11.1 :: 1 | 1129651464:721426

sub is_member
{
	my ($elem, @list) = @_;
	
	foreach $entry(@list) {
		if (($entry ne "") and ($elem eq $entry)) {
			return 1;
	    }
	}
	return 0;
}

while ($line ne '') {
	if ($line =~ /Available edges:/) {
		$line = <IN>;
		@links = ();

		while ($line ne "") {
			if ($line =~ /[ \t]*([^ ]*) -> ([^ ]*) :: ([^ ]*) / ) {
				$node1 = $1;
				$node2 = $2;
				$link = $3;

				if ($link > 0 and $node1 =~ /10.*/ and $node2 =~ /10.*/) {
					if ($node1 lt $node2) {
						$edge = "\t\"$node1\" -- \"$node2\" [style=bold];\n";
						$edge_tmp = "\t$node1 -- $node2\n";
					} else {
						$edge = "\t\"$node2\" -- \"$node1\" [style=bold];\n";
						$edge_tmp = "\t$node2 -- $node1\n";
					}
					if (is_member($edge, @links) == 0) {
						push(@links, $edge);
					}
					if (is_member($edge_tmp, @links_tmp) == 0) {
						push(@links_tmp, $edge_tmp);
					}
				} elsif ($link == -1 and $node1 =~ /10.*/ and $node2 =~ /10.*/) {
					if ($node1 lt $node2) {
						$edge = "\t\"$node1\" -- \"$node2\" [style=bold color=red];\n";
						$edge_tmp = "\t$node1 -- $node2\n";
					} else {
						$edge = "\t\"$node2\" -- \"$node1\" [style=bold color=red];\n";
						$edge_tmp = "\t$node2 -- $node1\n";
					}
					if (is_member($edge, @broken_links) == 0) {
						push(@broken_links, $edge);
					}
					if (is_member($edge_tmp, @broken_links_tmp) == 0) {
						push(@broken_links_tmp, $edge_tmp);
					}
				}
			}
			$line = <IN>;
		}
	}
	$line = <IN>;
}
print "\nBACKBONE LINKS:\n";

for ($i = 0; $i < @links_tmp; $i++) {
	if (is_member($links_tmp[$i], @broken_links_tmp)) {
		# the link in other direction is -1, ignore it
	} else {
		# the link is up in both directions
		print $OUT $links[$i];
		print $links_tmp[$i];
	}
}

# print all edges with -1
print "\tBroken links:\n";
print $OUT @broken_links;
print @broken_links_tmp;

print $OUT "}";
