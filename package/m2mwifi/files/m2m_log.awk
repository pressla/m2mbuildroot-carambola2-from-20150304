#!/usr/bin/awk -f
#m2m_log.awk
#Author: jingjin wang for imitrix GmbH, 2015
$1 == "Station" {
    station = $2
    while (getline > 0) {
        if ($1 == "inactive" && $2 == "time:") {
            inactive_time = $3
	} else if ($1 == "rx" && $2 == "bytes:") {
            rx_bytes = $3
	} else if ($1 == "rx" && $2 == "packets:") {
            rx_packets = $3
	} else if ($1 == "tx" && $2 == "bytes:") {
            tx_bytes = $3
	} else if ($1 == "tx" && $2 == "packets:") {
            tx_packets = $3
	} else if ($1 == "tx" && $2 == "retries:") {
            tx_retries = $3
	} else if ($1 == "tx" && $2 == "failed:") {
            tx_failed = $3
	} else if ($1 == "signal" && $2 == "avg:") {
            signal_avg = $3
	} else if ($1 == "tx" && $2 == "bitrate:") {
            tx_bitrate = $3
            tx_bitrate_unit = $4
	} else if ($1 == "rx" && $2 == "bitrate:") {
            rx_bitrate = $3
            rx_bitrate_unit = $4
	} else if ($1 == "mesh" && $2 == "llid:") {
            mesh_llid = $3
	} else if ($1 == "mesh" && $2 == "plid:") {
            mesh_plid = $3
	} else if ($1 == "signal:") {
            signal = $2
	} else if ($1 == "Toffset:") {
            toffset = $2
        }
	if (inactive_time != "" && rx_bytes != "" && rx_packets != "" && tx_bytes != "" && tx_packets != "" && tx_retries != "") {
	if (tx_failed != "" && signal != "" && signal_avg != "" && toffset != "" && tx_bitrate != "" && rx_bitrate != "") {
        	entries[++e] = "{\"Station\":\"" station "\",\"inactive_time\":\"" inactive_time " ms\",\"rx_bytes\":\"" rx_bytes "\""
		entries[e] = entries[e] ",\"rx_packets\":\"" rx_packets "\",\"tx_bytes\":\"" tx_bytes "\",\"tx_packets\":\"" tx_packets "\""
		entries[e] = entries[e] ",\"tx_retries\":\"" tx_retries "\",\"tx_failed\":\"" tx_failed "\",\"signal\":\"" signal " dBm\""
		entries[e] = entries[e] ",\"signal_avg\":\"" signal_avg " dBm\",\"Toffset\":\"" toffset " us\",\"tx_bitrate\":\"" tx_bitrate " " tx_bitrate_unit "\""
		entries[e] = entries[e] ",\"rx_bitrate\":\"" rx_bitrate " " rx_bitrate_unit "\"}"
        	break
	}
	}
    }
    station = inactive_time = rx_bytes = rx_packets = tx_bytes = tx_packets = signal = toffset = tx_bitrate_unit = ""
    rx_retries = tx_failed = signal_avg = tx_bitrate = rx_bitrate = rx_bitrate_unit = mesh_llid = mesh_plid = ""
}

END {
         cmd="date +%s"
         cmd|getline var1
         cmd="uci get wireless.mesh.mesh_id"
         cmd|getline var2
     print "["
     printf "{\"timestamp\":\"%s\",", var1
     printf "\"mesh_id\":\"%s\"},", var2
    if (e > 0) {
        printf "%s", entries[1]
        for (i = 2; i <= e; ++i) {
            printf ",\n%s", entries[i]
        }
        print ""
    }
    print "]"
}