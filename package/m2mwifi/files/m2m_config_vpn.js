//m2m_config_vpn.js
//Author: jingjin wang for imitrix GmbH, 2015
var httpoptions = {
    host: 'acc.ledwifi.de',
    port: 80,
    path: "/vpnrequest?mac="+process.env.vpnmac,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
};
var http = require("http");
var exec=require('child_process').exec;
var req = http.request(httpoptions, function(res)
{
    var output = '';
    res.setEncoding('utf8');

    res.on('data', function (chunk) {
        output += chunk;
    });

    res.on('end', function() {
        var obj = JSON.parse(output);
	if (obj.vpn == "up" && process.env.vpnup == "0") exec("/etc/init.d/softethervpnclient start");
	if (obj.vpn == "down" && process.env.vpnup == "1") exec("/etc/init.d/softethervpnclient stop");
    });
});

req.on('error', function(err) {
    //res.send('error: ' + err.message);
});
req.end();